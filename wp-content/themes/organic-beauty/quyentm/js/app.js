String.prototype.capitalize = function (){
	return this.charAt(0).toUpperCase() + this.slice(1);
}
function changeTitleSlider(currentElement)
{
	var set = currentElement.dataset;
	var color = set.color;
	var parent = $(currentElement).parent().parent().parent().parent().parent().parent();
	titleEl = parent.find('.post_content h3 a');
	var title =  set.productTitle + ' màu ' + color.replace('-', ' ').capitalize();
	parent.find('.post_content h3 a').text(title);
	parent.find('.quyentm-ajax-add-to-cart').attr('data-variation',set.variationId);
	parent.find('.buy-now').attr('data-variation',set.variationId);
}
(function($) {

	var app = {
		addToCart:function (data, element){
			 $.ajax({
	            type: 'post',
	            url: wc_add_to_cart_params.ajax_url,
	            data: data,
	            beforeSend: function (response) {
	            	element.text('Loading...')
	                element.addClass('disabled');
	            },
	            complete: function (response) {
	                element.removeClass('disabled');
	            },
	            success: function (response) {
	            	if(element.hasClass('buy-now')) {
	            		return window.location.assign('/cart')
	            	}

	            	if(response.fragments) {
	            		$('div.widget_shopping_cart_content').parent().html(response.fragments['div.widget_shopping_cart_content']);
	            		$('span.contact_cart_totals').html(response.fragments['span.ess-cart-content']);
	            		var s = document.querySelector('.ess-cart-content');
	            			s = s.innerText.split('-');
	            			s = parseInt(s[0]);
	            		$('.top_panel_cart_button').attr('data-items', s);
	            		var amount = document.querySelector('.woocommerce-mini-cart__total span.woocommerce-Price-amount');
	            		$('.top_panel_cart_button').attr('data-summa', $(amount).text())
	            		element.text('Đã thêm');
	            	} else {
	            		element.text('Thêm vào giỏ');
	            	}
	            },
	        });
		}
	}

	var $slider = $('.product-loop-slide-archive').bxSlider({
		onSliderLoad: function(currentIndex) {
			var currentElement = $('.post_thumb .product-link-slide')[currentIndex + 1];
			changeTitleSlider(currentElement);
		},
		onSlideBefore:function(slider, oldIndex, newIndex) {
			var currentElement = $('.post_thumb .product-link-slide')[newIndex + 1];
			changeTitleSlider(currentElement);
		}
	});


	$('.quyentm-ajax-add-to-cart').click(function (e){
		e.preventDefault();
		var variationId = $(this).attr('data-variation');
		var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: $(this).attr('data-product-id'),
            product_sku: '',
            quantity: $(this).attr('data-quantity')
        };

        if(parseInt(variationId) > 0) {
        	data = Object.assign({
        		variation_id: variationId
        	}, data)
        };
        app.addToCart(data, $(this));
		return false;
	});


})(jQuery);