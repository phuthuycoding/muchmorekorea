<?php
/**
 * Enqueue scripts and styles.
 */
function quyentm_scripts() {

	//wp_enqueue_style( 'quyentm-fullpage-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.css');
	//wp_enqueue_style( 'quyentm-bootstrap',    'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css');
	wp_enqueue_style( 'quyentm-bxslider', 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css');
	wp_enqueue_style( 'quyentm-custom-style', get_template_directory_uri() . '/quyentm/css/app.css');
	// wp_enqueue_style( 'quyentm-style-font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', [], '4.7.0');
	// wp_enqueue_style( 'quyentm-style-lightbox', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/css/lightbox.min.css', [], '2.11.1');
	// wp_enqueue_style( 'quyentm-lato-fonts', get_template_directory_uri() . '/css/font.css' );
	// wp_enqueue_style( 'quyentm-select2', get_template_directory_uri() . '/css/select2.min.css' );
	// wp_enqueue_style( 'quyentm-OwlCarousel2', get_template_directory_uri() . '/css/owl.carousel.min.css' );
	// wp_enqueue_style( 'quyentm-OwlCarousel2-themes', get_template_directory_uri() . '/css/owl.theme.default.min.css' );
	// wp_enqueue_style( 'quyentm-animations', get_template_directory_uri() . '/css/animations.min.css' );
	// wp_enqueue_style( 'quyentm-style', get_stylesheet_uri() );
	// wp_enqueue_style( 'quyentm-style-responsive', get_template_directory_uri() . '/css/responsive.css' );
    //wp_enqueue_script( 'jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.1/jquery.min.js', array(), '1.11.1', false );
	//wp_enqueue_script( 'quyentm-fullpage-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.min.js', array(), '2.6.6', false );
	// wp_enqueue_script( 'quyentm-OwlCarousel2', 'https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js', array(), '2.3.4', true );
	// wp_enqueue_script( 'quyentm-select2', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.full.min.js', array(), '4.0.7', true );
	// wp_enqueue_script( 'quyentm-animate', 'https://cdnjs.cloudflare.com/ajax/libs/css3-animate-it/1.0.3/js/css3-animate-it.min.js', array(), '1.0.3', true );
	// wp_enqueue_script( 'quyentm-lightbox2', 'https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.1/js/lightbox.min.js', array(), '2.11.1', true );
	wp_enqueue_script( 'quyentm-carousel', 'https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js', array(), '20151215', true );
	wp_enqueue_script( 'quyentm-app-js', get_template_directory_uri() . '/quyentm/js/app.js', array(), '20151215', true );
}

add_action( 'wp_enqueue_scripts', 'quyentm_scripts' );


add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	// unset($fields['billing']['billing_email']);
	return $fields;
}



add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'quyentm_woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'quyentm_woocommerce_ajax_add_to_cart');
        
function quyentm_woocommerce_ajax_add_to_cart() {

            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
            $variation_id = absint($_POST['variation_id']);
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
            $product_status = get_post_status($product_id);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }

                WC_AJAX :: get_refreshed_fragments();
            } else {

                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

                echo wp_send_json($data);
            }

            wp_die();
        }